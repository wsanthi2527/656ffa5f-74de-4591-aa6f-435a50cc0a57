import React from "react";
import Event from "./Event";
import { Typography } from "@material-ui/core";
import "../events.css";

const Events = ({ events, searchTerm }) => {
  return (
    <section>
      <Typography>
        <div className="event-cards">
          {events
            .filter((event) => event.title.toLowerCase().includes(searchTerm))
            .map((event) => {
              return <Event key={event._id} {...event} />;
            })}

          {/* {events.map((event) => {
            return <Event key={event._id} {...event} />;
          })} */}
        </div>
      </Typography>
    </section>
  );
};

export default Events;
