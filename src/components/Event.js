import React from "react";
import AddBoxIcon from "@mui/icons-material/AddBox";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { IconButton } from "@mui/material";

const IMG = "https://www.acadiate.com/images/Placeholder.png";

const Event = ({ _id, title, flyerFront, venue, startTime, endTime }) => {
  const start = { startTime };
  const end = { endTime };
  const location = { venue };

  let start2 = start.startTime;
  let end2 = end.endTime;

  let primaryIMG = flyerFront;

  console.log(primaryIMG);

  if (!primaryIMG) {
    primaryIMG = IMG;
  }

  if (typeof end2 !== "undefined") {
    const end3 = end2.split("T");
    const endDate = end3[0].replaceAll("-", ".");
    const endTime = end3[1].slice(0, 8);
    const end5 = endDate.split(".").reverse();
    end2 = end5[0] + "." + end5[1] + "." + end5[2] + ", " + endTime;
  }

  if (typeof start2 !== "undefined") {
    const start3 = start2.split("T");
    const startDate = start3[0].replaceAll("-", ".");
    const startTime = start3[1].slice(0, 8);
    const startDate2 = startDate.split(".").reverse();
    start2 =
      startDate2[0] +
      "." +
      startDate2[1] +
      "." +
      startDate2[2] +
      ", " +
      startTime;
  }

  const openTab = () => {
    window.open(location.venue.direction);
  };

  return (
    <article className="event-card">
      <div className="title">
        <h3>{title}</h3>
      </div>
      <img src={primaryIMG} alt={_id} />
      <div className="info">
        <p>
          <span>
            <LocationOnIcon
              sx={{ fontSize: 35 }}
              color="primary"
              onClick={openTab}
            />
          </span>
          {venue.name}
        </p>
        <p> Starts: {start2}</p>
        <p> Ends: {end2}</p>
        <IconButton className="addboxicon">
          <AddBoxIcon color="primary" fontSize="large" />
        </IconButton>
      </div>
    </article>
  );
};

export default Event;
